﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lerp : MonoBehaviour
{
    public Image fill;

    public Slider slider;

    void Start()
    {
        slider.value = 0;
        StartCoroutine(Filler());
    }

    void Update()
    {
        fill.color = Color.Lerp(Color.red, Color.green, slider.value);
    }

    IEnumerator Filler ()
    {
        yield return new WaitForSeconds(2f);
        while (true)
        {
            slider.value += Time.deltaTime;
            yield return null;
        }
    }
}


//lerpedColor = Color.Lerp(Color.green, Color.red, Time.time);
