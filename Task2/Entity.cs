﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Entity : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public float mass;

    public void OnBeginDrag(PointerEventData eventData)
    {
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.pointerCurrentRaycast.screenPosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        //     GetComponent<Collider2D>;
        mass = Random.Range(0.1f, 2.5f);
    }

    void Update()
    {
        Vector3 bigness = new Vector3(mass, mass, 1);
        gameObject.GetComponent<RectTransform>().localScale = bigness;
    }

    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (mass > collision.gameObject.GetComponent<Entity>().mass)
        {
            Debug.Log("Consume");
            //   collision.isActiveAndEnabled;
            Destroy(collision.gameObject);
            mass += collision.gameObject.GetComponent<Entity>().mass * 0.25f;       
        }
    }
}
